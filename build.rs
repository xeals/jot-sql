#[macro_use]
extern crate clap;

use std::env;
use clap::Shell;

include!("src/cli.rs");

fn main() {
    let outdir = match env::var_os("OUT_DIR") {
        None => return,
        Some(outdir) => outdir,
    };
    let mut app = cli();
    app.gen_completions("jot", Shell::Zsh, outdir)
}
