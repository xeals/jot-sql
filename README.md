# jot

Jot is a command-line tool to quickly take down what's on your mind. Currently it only offers storing, viewing, and removing your jots, but expansion is planned.

# Prerequisites

Jot is written in Rust and can be built using Rust's build tool, `cargo`. As such, `jot` depends on `cargo` in order to be built, as well as some additional libraries:

- SQLIte, as the choice of database;

## Ubuntu

```
sudo apt install cargo sqlite3 libsqlite3-dev
```

## Solus

```
sudo eopkg in cargo sqlite3 sqlite3-devel
```

# Configuring

Jot stores its database and configuration file in `$XDG_CONFIG_HOME/jot` (defaults to `$HOME/.config/jot`). The configuration file must be valid [toml](https://github.com/toml-lang/toml). Valid keys are:

- `db_path`: the path for the database file. Can use environment variables.
- `output_format`: a format for `jot list`. Valid substitution values are `%i` for the jot index and `%c` for the content.
- `index_padding`: how many digits to pad the jot index to with `jot list`.

See [the sample configuration file](resources/config.toml) for the defaults.

# Usage

Jot's commands can be accessed using `jot help`.

- `jot add` adds a new jot;
- `jot list` shows all your jots;
- `jot remove` deletes a jot;
- `jot edit` allows you to update a jot.

# Roadmap

See [TODOs.org](TODOs.org) for the full list.

- Entry and last-modified time stamps on each jot
- Reminder notifications using `inotify` or similar
