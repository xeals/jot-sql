extern crate shellexpand;
extern crate toml;
extern crate xdg;

use std::fs::File;
use std::io::Read;
use std::path::PathBuf;
use errors::*;

#[derive(Deserialize)]
struct RawConfig {
    db_path: Option<String>,
    output_format: Option<String>,
    index_padding: Option<i64>,
}

struct Config {
    db_path: PathBuf,
    output_format: String,
    index_padding: i64,
}

lazy_static! {
    static ref XDG_DIRS: xdg::BaseDirectories = xdg::BaseDirectories::with_prefix("jot")
        .expect("Couldn't get XDG base directory.");

}

macro_rules! invalid {
    ($s:expr) => (Err(ErrorKind::InvalidConfigError($s.to_string()).into()));
}

/// Reads a `config.toml` file. Intended for use with `convert_config`.
fn parse_config_file() -> Result<RawConfig> {
    if let Some(conf_path) = XDG_DIRS.find_config_file("config.toml") {
        // Use config values where available
        let mut contents = String::new();
        let mut conf_file = File::open(conf_path)?;
        let _ = conf_file.read_to_string(&mut contents);

        match toml::from_str(&contents) {
            Ok(conf) => Ok(conf),
            Err(e) => Err(e).chain_err(|| "failed to parse config"),
        }
    } else {
        Err("no config found.".into())
    }
}

/// Parses a `RawConfig` into a usable `Config`. Will supply default values for
/// configuration values not defined in the `config.toml`.
fn convert_config(conf: RawConfig) -> Config {
    let db_path = if let Some(raw_db_path) = conf.db_path {
        shellexpand::full(&raw_db_path)
            .expect("Failed to properly expand database path.")
            .to_string()
    } else {
        XDG_DIRS
            .place_config_file("jot.sqlite")
            .unwrap()
            .to_string_lossy()
            .to_string()
    };
    let format = match conf.output_format {
        Some(raw) => raw,
        None => "content:02} | {id}".to_string(),
    };
    let pad = match conf.index_padding {
        Some(raw) => raw,
        None => 2,
    };

    Config {
        db_path: PathBuf::from(db_path),
        output_format: format,
        index_padding: pad,
    }
}

/// Returns a `Config` with some or all default values, depending on if there
/// exists a user-defined `config.toml`.
fn config() -> Result<Config> {
    if let Ok(conf) = parse_config_file() {
        let conv = convert_config(conf);
        // Run the gauntlet of input checking
        if conv.index_padding < 0 {
            invalid!("index padding must be a non-negative integer")
        } else {
            Ok(conv)
        }
    } else {
        // Default config
        // TODO try and move this to a constant without having it be a different type.
        Ok(Config {
            db_path: XDG_DIRS.place_config_file("jot.sqlite").unwrap(),
            output_format: "{content:02} | {id}".to_string(),
            index_padding: 2,
        })
    }
}

// pub fn database_exists() -> bool {
//     // TODO Not sure this is the correct way to do this
//     // let conf = config().expect("Failed to verify database existence");
//     File::open(database_path().expect("Failed to check database")).is_ok()
// }

pub fn database_path() -> Result<PathBuf> {
    match config() {
        Ok(conf) => Ok(conf.db_path),
        Err(e) => Err(e).chain_err(|| "unable to read config"),
    }
}

pub fn output_format() -> Result<String> {
    match config() {
        Ok(conf) => Ok(conf.output_format),
        Err(e) => Err(e).chain_err(|| "unable to read config"),
    }
}

pub fn index_padding() -> Result<i64> {
    match config() {
        Ok(conf) => Ok(conf.index_padding),
        Err(e) => Err(e).chain_err(|| "unable to read config"),
    }
}
