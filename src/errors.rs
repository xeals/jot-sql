extern crate rusqlite;
extern crate toml;

use std;

error_chain! {
    types {
        Error, ErrorKind, ResultExt, Result;
    }
    foreign_links {
        Io(std::io::Error);
        Sqlite(rusqlite::Error);
        Toml(toml::de::Error);
    }
    errors {
        InvalidConfigError(t: String) {
            description("invalid configuration")
            display("invalid configuration: {}", t)
        }
    }
}
