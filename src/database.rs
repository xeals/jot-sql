extern crate rusqlite;

use config;
use errors::*;
use std::path::Path;

macro_rules! close_with_message {
    ($conn: expr) => (
        {
            if let Some((_, e)) = $conn.close().err() {
                Err(e.into())
            } else {
                Ok(())
            }
        });
    ($msg: expr, $conn: expr) => (
        {
            if let Some((_, e)) = $conn.close().err() {
                Err(e.into())
            } else {
                println!($msg);
                Ok(())
            }
        });
    ($msg: expr, $idx: expr, $conn: expr) => (
        {
            if let Some((_, e)) = $conn.close().err() {
                Err(e.into())
            } else {
                println!($msg, $idx);
                Ok(())
            }
        });
}

pub fn open() -> Result<rusqlite::Connection> {
    // HACK I tried to simplify this and made it worse.
    match config::database_path() {
        Ok(path) => {
            let c = rusqlite::Connection::open(path.as_path()).chain_err(
                || "unable to open database",
            )?;
            match c.execute("CREATE TABLE jot", &[]) {
                Ok(_) => {
                    c.execute("DROP TABLE jot", &[])?;
                    println!("[?] no database found; creating");
                    initialise(c)
                }
                Err(_) => Ok(c),
            }
        }
        Err(e) => Err(e).chain_err(|| "unable to open database"),
    }
}

pub fn open_from(path: &Path) -> Result<rusqlite::Connection> {
    if path.is_file() {
        let c = rusqlite::Connection::open(path).chain_err(|| "unable to open database");
        Ok(c.unwrap())
    } else {
        Err("Database file doesn't exist".into())
    }
}

fn initialise(conn: rusqlite::Connection) -> Result<(rusqlite::Connection)> {
    conn.execute(
        "CREATE TABLE IF NOT EXISTS jot (
         id INTEGER PRIMARY KEY AUTOINCREMENT,
         content TEXT NOT NULL
         )",
        &[],
    )?;

    Ok(conn)
}

pub fn insert_entry(jot: &str, conn: &rusqlite::Connection) -> Result<()> {
    match conn.execute("INSERT INTO jot (content) VALUES (?);", &[&jot]) {
        Ok(_) => Ok(()),
        Err(e) => Err(e.into()),
    }
}

pub fn remove_entry(index: &isize, conn: &rusqlite::Connection) -> Result<()> {
    match conn.execute("DELETE FROM jot WHERE id = ?", &[index]) {
        Ok(_) => Ok(()),
        Err(e) => Err(e.into()),
    }
}

pub fn update_entry(index: &isize, jot: &str, conn: &rusqlite::Connection) -> Result<()> {
    match conn.execute("UPDATE jot SET contents = ?1 WHERE id = ?2", &[&jot, index]) {
        Ok(_) => Ok(()),
        Err(e) => Err(e.into()),
    }
}

pub fn fetch_entry(index: &isize, conn: &rusqlite::Connection) -> Result<String> {
    match conn.query_row(
        "SELECT content FROM jot WHERE id = ?",
        &[index],
        |r| r.get(0),
    ) {
        Ok(query) => Ok(query),
        // Err(_) => Err(format!("Failed to find jot with index {}", index))?,
        Err(e) => Err(e).chain_err(|| "Failed to find jot"),
    }
}

// HACK `REINDEX` doesn't work the way I want it to, so the easiest way to
// reorganise entries is to temporarily create a new table and import the old
// data into it. This creates new 1-indexed IDs for each entry, and maintains
// the old IDs for update/removal purposes.
pub fn print_table(
    conn: &rusqlite::Connection,
    user_fmt: Option<&str>,
    user_pad: Option<&str>,
) -> Result<()> {
    // Migrate table for easier iterating
    conn.execute(
        "CREATE TABLE IF NOT EXISTS tmp(
         id INTEGER,
         content STRING,
         lookup INTEGER PRIMARY KEY AUTOINCREMENT
         )",
        &[],
    )?;
    conn.execute(
        "INSERT INTO tmp(id, content)
         SELECT id, content FROM jot",
        &[],
    )?;

    let rows: isize = conn.query_row("SELECT COUNT(*) FROM tmp", &[], |r| r.get(0))
        .expect("Failed to count rows in `print_table`");
    let format = match user_fmt {
        Some(fmt) => fmt.to_string(),
        None => config::output_format().unwrap(),
    };
    let padding = match user_pad {
        Some(pad) => pad.parse().unwrap(),
        None => config::index_padding().unwrap(),
    };

    for r in 1..rows + 1 {
        let cont: String = conn.query_row(
            "SELECT content FROM tmp WHERE lookup = ?",
            &[&r],
            |w| w.get(0),
        ).expect("Failed to fetch content in `print_table`");
        let id: isize = conn.query_row("SELECT id FROM tmp WHERE lookup = ?", &[&r], |w| w.get(0))
            .expect("Failed to fetch id in `print_table`");

        let fmt_id = format!("{id:0c$}", id = id, c = padding as usize);
        let fmt = format.replace("%i", &fmt_id).replace("%c", &cont);
        println!("{}", fmt);
    }

    conn.execute("DROP TABLE tmp", &[])?;
    Ok(())
}
