use clap::{Arg, App, AppSettings, SubCommand};

pub fn cli() -> App<'static, 'static> {

    // TODO refactor this
    let pos = Arg::with_name("NUMBER")
        .help("Position of the jot (from oldest to newest)")
        .required(true);
    let msg = Arg::with_name("msg")
        .help("Use the given <msg> as the jot content.")
        .short("m")
        .long("msg")
        .takes_value(true);
    let config = Arg::with_name("config")
        .help("Use the specified configuration file")
        .short("c")
        .long("config")
        .takes_value(true);
    let db = Arg::with_name("database")
        .help("Use the specified database (overrides configuration file)")
        .short("D")
        .long("db")
        .takes_value(true);

    App::new(crate_name!())
        .version(crate_version!())
        .author(crate_authors!())
        .about(crate_description!())
        .settings(&[AppSettings::SubcommandRequiredElseHelp,
                    AppSettings::VersionlessSubcommands])
        .subcommand(
            SubCommand::with_name("add")
                .about("Add a new jot")
                .arg(&config)
                .arg(&db)
                .arg(&msg)
        )
        .subcommand(
            SubCommand::with_name("edit")
                .about("Edit an existing jot")
                .arg(&config)
                .arg(&db)
                .arg(&msg)
                .arg(&pos),
        )
        .subcommand(
            SubCommand::with_name("remove")
                .alias("rm")
                .about("Remove a jot")
                .arg(&config)
                .arg(&db)
                .arg(&pos)
        )
        .subcommand(
            SubCommand::with_name("list")
                .alias("ls")
                .about("Show all current jots")
                .arg(&config)
                .arg(&db)
                .arg(
                    Arg::with_name("done")
                        .help("Show all done jots")
                        .short("d")
                        .long("done")
                )
                .arg(
                    Arg::with_name("format")
                        .help("Use the specified output format")
                        .short("f")
                        .long("format")
                        .takes_value(true)
                )
                .arg(
                    Arg::with_name("padding")
                        .help("Use the specified padding for the jot index")
                        .short("p")
                        .long("padding")
                        .takes_value(true)
                )
                .arg(
                    Arg::with_name("extended")
                        .help("Expand all jot contents")
                        .short("e")
                        .long("ext")
                )
        )
}
